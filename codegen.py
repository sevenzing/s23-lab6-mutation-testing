import itertools
from src.bonus_system import calculateBonuses

program = [None, 'nothing', 'Standard', 'Premium', 'Diamond']
amount = [0, 10000, 25000, 50000, 75000, 100000, 200000]


print('''
from bonus_system import calculateBonuses
import pytest

@pytest.mark.parametrize("program,amount,expected", [''')

for (p, a) in itertools.product(program, amount):
    expected = calculateBonuses(p, a)

    print(f'    ({p!r}, {a}, {expected}),')

print('''])

def test_bonus_system(program, amount, expected):
    result = calculateBonuses(program, amount)
    assert result == expected
''')